from simpleai.core.search.algorithms import DepthFirstSearchAlgorithm, BreadthFirstSearchAlgorithm, IDSAlgorithm, SearchProblem
from simpleai.core.generic import State
from simpleai.core.search.optimizers import GreedyOptimizer, AStarOptimizer
from simpleai.samples.search.buckets import BucketProblem

class BinaryProblem( SearchProblem ):
    def initial_state(self):
        return State( 0, 0, 0, 1, 0, 1 )
    
    def choose_state(self, queue):
        queue.sort( key = lambda state: sum( state.data ), reverse=True )
        return queue.pop( 0 )

    def successors(self, state):
        succ = []
        data = list( state.data )
        for index in range( 6 ):
            copy = data[:]
            copy[ index ] = 1 if copy[ index ] == 0 else 0
            succ.append( State( tuple( copy ) ) )
        return succ

    def is_valid(self, state):
        return True
    
    def is_solution(self, state):
        return state == ( 1, 1, 1, 1, 1, 1 )
        

if __name__ == "__main__":
    solver = BreadthFirstSearchAlgorithm()
    result = solver.search( BinaryProblem() )
    result.export( "Test" )
    print( result.deployed_number(), result.reached_number() )
    if result.solution_found():
        print( result.solution() )
        print([ str( src ) + str( dest ) for src, dest in result.path() ])