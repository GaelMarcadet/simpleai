from simpleai.core.generic import State
from simpleai.core.search.algorithms import SearchProblem, BreadthFirstSearchAlgorithm

class PCP( SearchProblem ):
    def initial_state(self):
        return State( "", "" )

    def successors(self, state):
        ( a, b ) = state.data
        return [
            State( a + "a", b + "baa" ),
            State( a + "ab", b + "aa" ),
            State( a + "bba", b + "bb" ),
        ]
    
    def is_solution(self, state):
        ( a, b ) = state.data
        return a == b and a != ""