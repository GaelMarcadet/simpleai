from simpleai.core.search.algorithms import SearchProblem
from simpleai.core.generic import State

class BucketProblem( SearchProblem ):
    ''' Bucket problem searchs a solution when there is specific quantity of water in solution. '''
    def __init__(self):
        self.na = 5
        self.nb = 2
        self.ia = 5
        self.ib = 0

    def initial_state(self):
        ''' The initial state. '''
        return State( self.ia, self.ib )

    def successors(self, state):
        ''' Successors. '''
        a, b = state
        succ = [
            # empty one 
            State( 0, b ),
            State( a, 0 ),            
        ]
        # one to another: A to B
        atob = State( max( 0, a - ( self.nb - b ) ), min( self.nb, a + b )  ) # a -> b
        if atob != state:
            succ.append( atob )

        # on to another: B to A
        btoa = State( min( self.na, a + b ), max( 0, b - ( self.na - a ) ) )
        if btoa != state:
            succ.append( btoa )
        
        return succ

    
    def is_valid(self, state):
        ''' Returns True if state is value, False otherwise. '''
        return True

    def is_solution(self, state):
        ''' Returns True if state is a solution, False otherwise. '''
        return state == ( 0, 1 )


class BBucketProblem( SearchProblem ):

    def __init__(self):
        self.na = 5
        self.nb = 2
        self.ia = 5
        self.ib = 0

    def initial_state(self):
        return State( self.ia, self.ib )

    def successors(self, state):
        a, b = state.data
        return [
            # empty one 
            State( 0, b ),
            State( a, 0 ),
            # fill one
            State( self.na, b ),
            State( a, self.nb ),
            # one to another
            State( max( 0, a - ( self.nb - b ) ), min( self.nb, a + b )  ), # a -> b
            State( min( self.na, a + b ), max( 0, b - ( self.na - a ) ) ),
            
        ]

    def predecessors( self, state : State ):
        return self.successors(state)

    def is_valid(self, state):
        return True

    def goal_state(self):
        return State( 4, 0 )