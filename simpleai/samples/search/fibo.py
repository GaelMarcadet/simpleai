from simpleai.core.generic import State
from simpleai.core.search.algorithms import SearchProblem

class FiboProblem( SearchProblem ):
    def initial_state(self):
        return State( 10 )

    def is_solution(self, state):
        return state == 0
        
    def is_valid(self, state : State):
        return 0 <= state
    
    def successors( self, state : State ):
        return [ 
            State( state.data - x  )
            for x in range( 1, 3 ) 
        ]


# Let's define the fibonacci serie
# Notice that State object can be used like an interger here, thanks of operator override
# which allows to manipulate him like your provided data
class AStarFiboProblem( SearchProblem ):
    def initial_state(self):
        return State( 10, g=10, h = 0 ) # give g and h, a*'s properties

    def is_solution(self, state):
        return state == 0 
        
    def is_valid(self, state : State):
        return 0 <= state
    
    def successors( self, state : State ):
        return [ 
            # Access to g and h properties
            State( state - x, g = state.get( 'g' ) + x, h = state.get( 'h' ) - x )
            for x in range( 1, 5 ) 
        ]