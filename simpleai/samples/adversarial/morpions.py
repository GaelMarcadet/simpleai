from simpleai.core.generic import State
from simpleai.core.adversarial.algorithms import MinMaxAlgorithm, AlphaBetaAlgorithm, AdversarialHeuristic, AdversarialMove, AdversarialProblem

# we assign a number for each player
CROSS = 1
ROUND = -1
EMPTY = 0
class MorpionsGame:
    def __init__( self, init_with : [int] = None ):
        # let's the opportunity to initiate the game with anything you want
        if init_with is None:
            self.plate = [ EMPTY for i in range( 9 ) ]
        else:
            self.plate = init_with

    def cross( self, x, y ):
        ''' Puts a cross at specified position. '''
        copy = MorpionsGame()
        copy.plate = self.plate[:]
        copy.__set( x, y, CROSS )
        return copy

    def round( self, x, y ):
        ''' Puts a round at specified positions. '''
        copy = MorpionsGame()
        copy.plate = self.plate[:]
        copy.__set( x, y, ROUND )
        return copy

    def equals( self, x, y, value ):
        ''' Returns True if cell at specified position is equals to given value. '''
        return self.plate[ 3 * x + y ] == value

    def __get( self, x, y ):
        ''' Returns cell's value located at specified positions. '''
        return self.plate[ 3 * x + y ]

    def __set( self, x, y, value ):
        ''' Change cell's value located at specified positions. '''
        self.plate[ 3 * x + y ] = value

    
    def playable( self ):
        ''' Returns playable positions as a list of tuple which contains two integers x and y. '''
        playable_coords = []
        for x in range( 3 ):
            for y in range( 3 ):
                if self.equals( x, y, EMPTY ):
                    playable_coords.append( (x, y) )
        return playable_coords

    def someone_has_won( self ):
        ''' Returns True if someone has won, False otherwise. '''
        # a solution is founded if there is a three-length cross or round line or diagonale
        # checks solutions on line
        for x in range( 3 ):
            index = 3 * x
            if self.plate[ index ] == self.plate[ index + 1 ] and self.plate[ index + 1 ] == self.plate[ index + 2 ] and self.plate[ index ] != EMPTY:
                return True

        # checks solutions on column
        for y in range( 3 ):
            if self.plate[ y ] == self.plate[ 3 + y ] and self.plate[ 3 + y ] == self.plate[ 6 + y ] and self.plate[ y ] != EMPTY:
                return True

        # checks solutions in diagonale
        if self.plate[ 0 ] == self.plate[ 4 ] and self.plate[ 4 ] == self.plate[ 8 ] and self.plate[ 0 ] != EMPTY:
            return True
        
        if self.plate[ 2 ] == self.plate[ 4 ] and self.plate[ 4 ] == self.plate[ 6 ] and self.plate[ 2 ] != EMPTY:
            return True

        return False
    
    def is_playable( self ):
        ''' Returns True if game is playable, or in other words if at least one cell is empty. '''
        return self.playable() != []
    
    def is_end( self ):
        ''' Returns True if game is ended, False otherwise. '''
        # the game is end when someone has won
        # of if no one cell is playable
        return self.someone_has_won() or not self.is_playable()

    def ask_playable( self ) -> ( int, int ):
        playable_cells = self.playable()
        # display playable cells
        index = 1
        for x in range( 3 ):
            for y in range( 3 ):
                if self.equals( x, y, EMPTY ):
                    print( f"{index} ", end="" )
                    index += 1
                else:
                    print( 'X ' if self.equals( x, y, CROSS ) else 'O ', end="" )
            print( "\n", end = "" )
                

        # asks to pick playabel cell
        while True:
            try:
                cell_index = int( input( "Choose a playable cell: " ) ) - 1
                return playable_cells[ cell_index ]
            except KeyboardInterrupt:
                exit( 0 )
            except Exception:
                print( "Game: Not good value" )

        
        
    
    
        
    def __str__( self ):
        res = []
        for x in range( 3 ):
            for y in range( 3 ):
                cell = self.__get( x, y )
                if cell == EMPTY:
                    res.append( '. ' )
                elif cell == CROSS:
                    res.append( 'X ' )
                elif cell == ROUND:
                    res.append( 'O ' )
            res.append( "\n" )
        return "".join( res )


# MAX -> CROSS
# MIN -> ROUND

class MorpionsProblem( AdversarialProblem ):
    def initial_state(self):
        # Returns a new states which is a new Morpions game
        return State( MorpionsGame() )

    def successors( self, state, max_player ):
        succ = []
        for playable in state.data.playable():
            x, y = playable
            if max_player:
                succ.append( State( state.data.cross( x, y ) ) )
            else:
                succ.append( State( state.data.round( x, y ) ) ) 
        return succ
    
    def is_solution( self, state, max_player ):
        return state.data.is_end()

class MorpionsHeuristic( AdversarialHeuristic ):
    def score(self, state : State, max_player):
        game : MorpionsGame = state.data
        if game.someone_has_won():
            return -1 if max_player else 1 / state.depth
        else:
            return 0

def play_morpions( algorithm : str, nb_game : int = 1, human_start : bool = False ):
    problem = MorpionsProblem()
    
    if algorithm == "minmax":
        solver = MinMaxAlgorithm( problem, MorpionsHeuristic() )
    elif algorithm == "alphabeta":
        solver = AlphaBetaAlgorithm( problem, MorpionsHeuristic() )
    else:
        raise Exception( "Undefined algorithm " + str( algorithm ) )

    for game_index in range( nb_game ):
        print( f"================= game {game_index + 1} on {nb_game} ======================" )
        game : MorpionsGame = problem.initial_state().data
        end = False
        human_turn = human_start
        while not end:
            print( game )
            # checks if the game is end or not
            if game.is_end():
                if game.someone_has_won():
                    if human_turn:
                        print( "IA: I'm sorry, but you lose :(" )
                    else:
                        print( "IA: Wow, great job !" )
                else:
                    print( "IA: It's seems there is no winner..." )
                end = True
                break

            # play the game
            if human_turn:
                print( "-------------- Your turn -----------------" )
                x, y = game.ask_playable()
                game = game.round( x, y )
            else:
                print( "--------------  IA turn  -----------------" )
                print( "IA: Let me thinking about my next move..." )
                picked_move : AdversarialMove = solver.best_move( game )
                next_game : MorpionsGame = picked_move.move() # move returned data object
                game.plate = next_game.plate[:]
                print( "IA: Eureka !" )
                print( f"IA: I've explored {picked_move.explored_nodes()} nodes to found the solution !" )
            
            human_turn = not human_turn

from random import choice
if __name__ == "__main__":
    play_morpions( "alphabeta", nb_game=1, human_start=False )
    

