from simpleai.core.generic import State
from simpleai.core.adversarial.algorithms import AlphaBetaAlgorithm, MinMaxAlgorithm, AdversarialHeuristic, AdversarialProblem


class StickProblem( AdversarialProblem ):
    def initial_state(self):
        return State( 6 )

    def is_solution( self, state, max_player ):
        return state == 1
    
    def successors( self, state, max_player ):
        if state == 2:
            return [ State( state - 1 ) ]
        else:
            return [ State( state - 1 ), State( state - 2 ) ]
    
class StickHeuristic( AdversarialHeuristic ):
    def score( self, state : State, max_player : bool ):
        return -1 if max_player else 1 

from random import shuffle
def play_sticks( algorithm : str, nb_game : int = 1, human_start : bool = True, nb_sticks : int = 6 ):
    human_score, computer_score = 0, 0
    problem = StickProblem()
    if algorithm == "minmax":
        solver = MinMaxAlgorithm( problem, StickHeuristic() )
    elif algorithm == "alphabeta":
        solver = AlphaBetaAlgorithm( problem, StickHeuristic() )
    else:
        raise Exception( "Undefined algorithm " + str( algorithm ) )

    for game in range(  1, nb_game + 1 ):

        # defines player
        # True means that is a human turn, False is the AI turn
    
        players = [ human_start, not human_start ] 

        # prepare game's properties
        human_turn = players[ 0 ]
        end = False
        sticks = nb_sticks

    
        print( f"============ game {game} on {nb_game} ==============" )
        print( f"Your score: {human_score} - AI Score: {computer_score}" )
        print( f"----------------------------------------------------")
        while not end:
            print( f"Game: Current remaining sticks: {sticks} sticks" )
            if sticks == 1:
                print( '--------------------------------------------------' )
                if human_turn:
                    print( "IA: I'm sorry, but you lose :(" )
                    computer_score += 1
                else:
                    print( "IA: Wow, great job !" )
                    human_score += 1
                end = True
                break

            if human_turn:
                if sticks == 2:
                    print( "Game: There is only two remaining sticks, you must pick just one !" )
                else:
                    while True:
                        try:
                            picked_sticks = int( input( f"Game: Remaining {sticks} sticks, are you picking 1 or 2 picks ? " ) )
                            sticks = sticks - picked_sticks
                            break
                        except KeyboardInterrupt:
                            print( "IA: See you later !" )
                            exit( 1 )
                        except Exception:
                            print( "Game: You must enter 1 or 2: " )
            else:
                previous_stick = sticks
                sticks = solver.best_move( sticks ).move()
                print( f"IA: I've taken {previous_stick - sticks} sticks !" )
            
            human_turn = not human_turn

        print( f"=====================================" )
        print( f"Your score: {human_score}" )
        print( f"AI score: {computer_score}" )

