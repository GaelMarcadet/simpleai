#############################################################
# File: Optimizers.py
# Description: 
# An optimizer deals with an heuristic.
# An heuristic score a state.
#############################################################

from simpleai.core.generic import State
from simpleai.core.search.algorithms import SearchProblem

class Heuristic:
    def score( self, state : State ):
        pass

class CostHeuristic( Heuristic ):
    def score( self, state : State ):
        return state.cost

class Optimizer( SearchProblem ):
    def __init__( self, optimized_problem : SearchProblem ):
        self.problem = optimized_problem

    def initial_state(self):
        return self.problem.initial_state()

    def successors(self, state):
        return self.problem.successors( state )
    
    def is_solution( self, state ):
        return self.problem.is_solution( state )
    
    def is_valid(self, state):
        return self.problem.is_valid( state )
    
    def choose_state(self, queue):
        return self.problem.choose_state( queue )

class GreedyOptimizer( Optimizer ):
    def __init__(self, optimized_problem, heuristic : Heuristic, reverse : bool = False ):
        super().__init__(optimized_problem )
        self.reverse = reverse
        self.heuristic = heuristic

    def choose_state(self, queue):
        queue.sort( key = lambda state : self.heuristic.score( state ), reverse = self.reverse )
        return queue.pop( 0 )


class AStarOptimizer( Optimizer ):
    def __init__(self, problem, g : Heuristic, h : Heuristic):
        super().__init__(problem)
        self.g = g
        self.h = h
        self.score = lambda state: g.score( state ) + h.score( state )

    def initial_state(self):
        state = self.problem.initial_state()
        return state

    def choose_state(self, queue):
        queue.sort( key = lambda state: self.score( state ) )
        return queue.pop( 0 )

class DepthHeuristic( Heuristic ):
    def score( self, state : State ):
        return state.depth



        