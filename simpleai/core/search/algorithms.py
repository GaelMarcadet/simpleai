#####################################################
# File: algorithms.py
# Description: Implementation of algorithms used 
# to find a solution at a problem.
#####################################################

from typing import Mapping
from math import inf
from simpleai.core.generic import State
from abc import ABC, abstractmethod

class SearchProblem( ABC ):
    @abstractmethod
    def successors( self, state : State ):
        ''' Returns the successors of a state. '''
        pass

    @abstractmethod
    def is_solution( self, state : State ) -> bool :
        ''' Returns True if state is a solution, False otherwise. '''
        pass

    def is_valid( self, state : State ) -> bool:
        ''' Returns True if state is valid, False otherwise. '''
        return True

    @abstractmethod
    def initial_state( self ):
        ''' Returns the initial state of the problem. '''
        pass

    def choose_state( self, queue : [ State ] ):
        ''' 
        Chooses and returns the selected state to continue the search.
        '''
        return queue.pop( 0 )


class BidirectionnalProblem( SearchProblem ):
    ''' 
    Interface for bidirectionnal problem.
    The interface is for a specific use case and a specific kind of problems.
    '''
    def predecessors( self, state : State ):
        ''' Returns predecessors of a state. '''
        pass

    def goal_state( self ) -> State:
        ''' Returns the goal state of the search. '''
        pass


class Solution:
    def __init__(self, solution : State, path):
        import warnings
        warnings.warn( "deprecated", DeprecationWarning )
        self.solution : State = solution
        self.path = path
    
    def get_solution( self ) -> State:
        return self.solution

    def get_path( self ):
        return self.path
    
    def __str__( self ):
        return str( self.solution ) + " with a cost of " + str( self.solution.cost )

class Optional: 
    def __init__( self, data = None ):
        import warnings
        warnings.warn( "deprecated", DeprecationWarning )
        self.data = data

    def has_data( self ) -> bool:
        return self.data is not None

    def get_data( self ):
        return self.data

    def __str__(self):
        return str( self.data ) if self.has_data() else "None"


class SearchResult:
    def __init__( self, solution, initial, sons = {}, parent = {}, reached = {}  ):
        self.__solution = solution
        self.__sons = sons
        self.__parent = parent
        self.__reached_order = reached
        self.__initial = initial
        self.__deployed_state_number = sum([ 1 if discover != -1 else 0 for state, discover in reached.items() ])
        self.__reached_state_number = len( reached )

    def solution_found( self ) -> bool:
        ''' Returns True if a solution has been founded. False otherwise. '''
        return self.__solution is not None
    
    def solution_depth( self ) -> int:
        if self.solution_found():
            return self.__solution.depth
        else:
            return -1

    def solution( self ) -> State:
        ''' Returns the solution state founded by the algorithm. '''
        return self.__solution

    def deployed_number( self ) -> int:
        ''' Returns the number of nodes selected by the algorithm to perform the search. '''
        return self.__deployed_state_number
    
    def reached_number( self ) -> int:
        ''' 
        Returns the number of nodes detected by the algorithm. 
        It's represent the number of deployed nodes and their sons.
        '''
        return self.__reached_state_number

    def path( self ) -> [ (State, State) ]:
        ''' Returns the path from the initial state to the final state. '''
        state = self.__solution
        father = self.__parent[ state ]
        path = []
        while father is not None:
            path.insert( 0, (father, state) )
            father, state = self.__parent[ father ], father
        return path

    def export( self, filename : str ):
        def node_content( state ):
            return f' {state}\nord:{self.__reached_order[ state ]} dep:{state.depth}'
        ''' Exports the tree search as a pdf format. '''
        # creates graph
        from graphviz import Digraph
        dot = Digraph(comment='Explored graph')

        # browse each node and display successors
        for state, successors in self.__sons.items():
            src =   str( state ) + " " + str( self.__reached_order[ state ])
            dot.node( src, style = "filled" if self.__reached_order[ state ] != -1 else None, color="gray" )
            for succ in set(successors):
                dst =  str( succ ) + " " + str(self.__reached_order[ succ ])
                dot.node( dst, style = "filled" if self.__reached_order[ succ ] != -1 else None, color="gray" )
                dot.edge( src, dst )

        i = str( self.__initial ) + " " + str( self.__reached_order[ self.__initial ] )
        dot.node( i, color='green', style= "filled" )
        if self.solution_found():
            s = str( self.__solution ) + " " + str( self.__reached_order[ self.__solution ])
            dot.node( s ,color='lightblue2', style= "filled" )
        dot.render( filename, view=True )
        
        # delete temporary file
        import os
        os.remove( filename )

    
        

class SearchAlgorithm: 
    def add_state_in_queue( self, state : State, queue ):
        ''' Adds the state in the queue. '''
        pass

    def add_states_in_queue( self, states : [ State ], queue : [ State ] ):
        raise Exception( "Abstract method cannot be called" )

    def valid_depth( self, depth ):
        ''' 
        Returns True if given depth is valid or not.
        This method is usefull to avoid to search a solution too deeper.
        '''
        return True


    def search( self, problem : SearchProblem, depth_limit : int = None ) -> SearchResult:
        ''' 
        Searchs a solution for a given problem.
        This method return a SearchResult object which contains all data about the search.
        To avoid very long (or often infinite) search, you're free to specify a depth limit.
        '''
        def valid_depth( depth ): return depth <= depth_limit if depth_limit is not None else True

        initial_state : State = problem.initial_state()
        initial_state.depth = 0
        if initial_state.cost is None:
            initial_state.cost = 0


        queue = [ initial_state ]
        sons = {}
        parent = { initial_state: None }
        reached = { initial_state : 0 }
        order = 1

        #  returns initial state if solution
        if problem.is_solution( initial_state ): 
            return SearchResult( initial_state, initial_state, sons, parent, reached )

        while queue != []:
            # pop the firt stack of the list and get his successors.
            # All successors not visited yet and valid state and keept.
            state : State = problem.choose_state( queue )
            reached[ state ] = order
            order += 1
            

            # if successor is a valid solution, returns him
            if problem.is_solution( state ):
                sons[ state ] = []
                return SearchResult( state, initial_state, sons, parent, reached )
            
            next_depth = state.depth + 1

            if not valid_depth( next_depth ):
                continue

            # updates sucessors
            successors = [ succ for succ in problem.successors( state ) if problem.is_valid( succ ) and succ not in reached ]
            sons[ state ] = successors

            for succ in successors:
                reached[ succ ] = -1
                succ.depth = next_depth

                # links successor with a parent
                parent[ succ ] = state

                # updates depth and cost of successor
                # succ.cost = state.cost + 1 if succ.cost is None else state.cost + succ.cost

                # if not a solution, add him to the stack
            queue = self.add_states_in_queue( successors, queue )
                    
        # at this point, each state have been explored and no one is a solution
        # so returns empty
        return SearchResult( None, initial_state, sons, parent, reached )
                

class BidirectionalSearch:
    '''
    Search algorithm method which perform a bidirectional search in search space.
    DISCLAIMER: This method is still under developpement, if you are faced with a bug, please report him.
    '''

    def build_path( self, initial_state, goal_state : State, parents ):
        state = goal_state
        father = parents[ state ]
        path = []
        while father is not None:
            path.insert( 0, (father, state) )
            father, state = parents[ father ], father
        return path
            

    def search( self, problem : BidirectionnalProblem):
        initial_state = problem.initial_state()
        goal_state = problem.goal_state()

        successors = [ initial_state ]
        predecessors = [ goal_state ]

        reached_down = [ initial_state ]
        reached_up = [ goal_state ]

        parents = { initial_state: None }

        down = True

        while True:
            down = not down
            if down:
                state = successors.pop(0)
                for succ in problem.successors( state ):
                    if problem.is_valid( succ ):
                        reached_down.append( succ )
                        parents[ succ ] = state

                        if succ in reached_up:
                            return self.build_path( initial_state, goal_state, parents )

                        successors.insert( 0, succ )
            else:
                state = predecessors.pop(0)
                for pred in problem.predecessors( state ):
                    if problem.is_valid( pred ):
                        reached_up.append( pred )
                        parents[ state ] = pred 

                        if pred in reached_down:
                            return self.build_path( initial_state, goal_state, parents )

                        predecessors.insert( 0, pred )
                    




class BreadthFirstSearchAlgorithm( SearchAlgorithm ):
    ''' Search algorithm which performs a search using breadth first approach. '''
    def add_state_in_queue(self, state : State, stack):
        stack.append( state )

    def add_states_in_queue( self, states : [ State ], queue : [ State ] ):
        return queue + states

class DepthFirstSearchAlgorithm( SearchAlgorithm ):
    ''' Search algorithm which performs a search using depth first approach. '''
    def add_state_in_queue(self, state : State, queue):
        queue.insert( 0, state )

    def add_states_in_queue( self, states : [ State ], queue : [ State ] ):
        return states + queue

class IDSAlgorithm( DepthFirstSearchAlgorithm ):
    ''' 
    Search algorithm which performs a search using depth first approach.
    However, a depth limit avoid infinite search. 
    '''
    def increase_depth_limit( self, depth ):
        return depth * 10

    def search(self, problem : SearchProblem, depth_limit=None):
        depth = 1
        while True:
            result = super().search( problem, depth_limit = depth )
            if result.solution_found():
                return result

            # increase depth and return no solution if depth limit reached
            depth = self.increase_depth_limit( depth )
            if depth_limit is not None and depth_limit <= depth:
                return result