# Simple AI

SimpleAI is a (simple) library used to solve a problem or search a best solution using artificial intelligence algorithms.

Features implemented are:
- Search a solution in a big search space using BreadthFirst, DepthFrist or IncrementalDepth algorithms using optimizers like BFS or AStar and custom heuristics.
- Search to defeat a player in an 1v1 game using MinMax and AlphaBeta algorithms.

Features implemented in future version:
- Search a solution for a problem using local search methods (simple local search, local beam search, simulated recuit).


**DISCLAIMER: This library is still in developpement. Please report bugs if you have any problem.**

# Installation
There is two ways to install aisimple on your laptop is to use `pip3`.
Follows these few simple steps and you'll be done !

**Warning: To work properly, this library requires xdot script installed in your system.**

## Install from pip (recommanded)
The easiest way to install this library is to use *pip3*.
```
pip3 install -i https://test.pypi.org/simple/ simpleai
```

## Install from sources
If pip3 doesn't work, it's possible to install it from sources.
```
git clone https://gitlab.com/GaelMarcadet/simpleai.git
cd simpleai
pip3 install -e .
```
That's all !


# Documentation

## Search

### SearchAlgorithm
- `search( problem : SearchProblem ) -> SearchResult`: Search a solution to your problem.

### SearchProblem
- `initial_state() -> State`: Returns the initial state of the problem
- `successors( state : State ) -> [ State ]`: Returns the list of state's successors
- `is_solution( state : State ) -> Bool`: Returns True if the state is a solution, False otherwise.
- `is_valid( state : State ) -> Bool`: Returns True if state is valid for your problem, False otherwise.

### SearchHeuristic
- `score( state : State ) -> number`: Returns the state's score defined by your heuristic.

### SearchResult
- `solution_found() -> bool`: Returns True if a solution was found, False otherwise.
- `solution() -> State`: Returns solution found. 
- `solution_depth() -> int`: Returns found solution's depth.
- `deployed_number() -> int`: Returns number of nodes deployed (or selected in other words) by algorithm.
- `reached_number() -> int`: Returns number of nodes discover by algorithm. Include nodes not explored yet a the end of algorithm.
## Adversarial

### AdversarialAlgorithm
- `best_move( state : State, max_player : bool, depth_limit = inf ) -> AdversarialMove`: Search the best move to perform.

### AdversarialMove
- `move_state() -> State`: Get computed state object.
- `move_score() -> int`: Get computed score related to computed move.
- `move()`: Same as `move_state` but returned data object instead.



### AdversarialHeuristic
- `score( state : State, max_player : bool ) -> number`: Returns the state's score defined by your heuristic.

### AdversarialProblem
- `initial_state() -> State`: Returns the initial state of the problem
- `successors( state : State, max_player : bool ) -> [ State ]`: Returns the list of state's successors
- `is_solution( state : State, max_player : bool ) -> Bool`: Returns True if the state is a solution, False otherwise.
- `is_valid( state : State, max_player : bool ) -> Bool`: Returns True if state is valid for your problem, False otherwise.

# Explanation et Samples

## Project Architecture
The project has been divided in two distincts parts.
The first one is **core** section, which contains all ai algorithms, optimizers, etc...
The last one is **samples** which contains samples of problems to start easily.

```
.
├── core
│   ├── adversarial
│   │   └── algorithms.py
│   ├── generic.py
│   └── search
│       ├── algorithms.py
│       └── optimizers.py
└── samples
    ├── adversarial
    │   └── sticks.py
    └── search
        ├── buckets.py
        ├── fibo.py
        ├── hanoi.py
        ├── human.py
        └── post_correspondance.py
```

If you have developped an algorithm and you want to test him, checks this code:
```py
from simpleai.core.search.algorithms import BreadthFirstSearchAlgorithm, SearchResult
from simpleai.samples.search.buckets import BucketProblem


if __name__ == "__main__":
    solver = BreadthFirstSearchAlgorithm()
    result : SearchResult = solver.search( BucketProblem() )
    # Do some stuff ...
```


## State

A `State` object is a configuration of a problem, designed to be easily manipulated.
This object is central in the library, because she manipulates this object everywhere a configuration a problem.
We move one to another by using successors function, defined in problem definition.

States object has been defined to encapsulate problem's configuration complexity.

For instance, if you want to creates state's successors where state encapsulate an integer:
```py
# boring way...
def successors( state : State ) -> [ State ]:
    return [
        State( state.data + 1 ),
        State( state.data + 2 ),
    ]

# best way
def successors( state : State ) -> [ State ]:
    return [
        State( state + 1 ),
        State( state + 2 ),
    ]

# coming soon :)
def successors( state : State ) -> [ State ]:
    return [
        state + 1,
        state + 2,
    ]
```

## Solve a search problem
One of the feature implemented in this library is to search a solution to a problem.

### Step 1: Create a problem

To define a problem, creates a class which inherites from `SearchProblem`.
Four methods must be implemented explained in documentation part.
Let's implement slightly different *Hanoi towers problem*, where all disk have the same dimensions.

```py
class HanoiProblem( SearchProblem ):
    ''' Hanoi problem implementation using Problem interface. '''

    def initial_state(self):
        ''' Returns the initial state of the problem. '''
        # At the start all disks are to the left positions
        return State( 3, 0, 0 )
    
    def successors( self, state : State ):
        ''' Returns successors of the state. '''
        ( a, b, c ) = state.data
        return [
            State( a - 1, b + 1, c ),
            State( a - 1, b, c + 1 ),

            State( a + 1, b - 1, c ),
            State( a, b - 1, c + 1 ),

            State( a + 1, b, c - 1 ),
            State( a, b + 1, c - 1 ),
        ]

    def is_valid(self, state : State):
        ''' Elegates illegal states from the tree search. ''' 
        state_sum = sum( state.data )
        ( a, b, c ) = state.data
        return 0 <= state_sum and state_sum <= 3 and 0 <= a and 0 <= b and 0 <= c

    def is_solution(self, state : State):
        ''' Returns True if given state is the solution, False otherwise. '''
        # here we want to put all disks in the left positions
        return state == ( 0, 0, 3 )
```

### Step 2: Found a solution using a SearchAlgorithm

Actually, three search algoritms are available.
The BreadthFirstSearchAlgorithm, DepthFirstSearchAlgorithm and his extension, the IDSAlgorithm.
All of them can search a solution for a problem using `search( problem : Problem ) : SearchResult` method.

It returns a `SearchResult` object which is more like a report of the research.
With him, you're allow to check if a solution has been found, print her, get some extra data and finally, display the tree search ( currenlty in pdf format only ).

Let's see an example of it.
```py
search_engine = BreadthFirstSearchAlgorithm() # Let's execute breadth first search ...
result = search_engine.search( HanoiProblem() ) # ... on hanoi problem defined earlier
result.export( "HanoiTreeSearch" )
if result.solution_found():
    print( result.solution() ) # Displays founded solution
    # display path from initial to solution state
    for src, dest in result.path:
        print( ( src, dest) )
else:
    # when no solution has been found (not called here)
    print( "No solution found ! :(" )
```


### Step 3: Speed up problem resolution using Optimizers and Heuristic

Let's take an example, the N-puzzle called Taquin.

With native exploration, puzzle solving could be very long to solve. However, we are able to help search algorithm to pick the best next move to be closer to the solution.

To do that, we can use semantic approach of the problem, called **Heuristic**, that we give to an **Optimizer**, which is an extension of a problem to solve more quickly the initial problem.

#### Implemented Heuristics
Few heuristics are already implemented but a new one must be created for each problem developped by your own.
There are:
- **DepthHeuristic:** Heuristic which select a state by his depth.
- **CostHeuristic:** Heurist wich select a state by his cost.

#### Create an custom search Heuristic

Create your own heuristic is very simple. Just create a new class which inherit from the `SearchHeuristic` class, and implement the only one method `score( State ) -> number`.

Let's see the implementation of the `DepthHeuristic` class.

```py
from simpleai.core.search.optimizers import SearchHeuristic
class DepthHeuristic( SearchHeuristic ):
    def score( self, state : State ):
        return state.depth
```

#### Available Optimizers
Instead to search all states which could be very long, we can help solver to perform the exploration
by selecting the best states. The entity who do that is called an *Optimizer*.

Some optimizers are available:
- **GreedyOptimizer:** Select the best score, depending of an heuristic.
- **AStarOptimizer:** Select the best compromise between estimated distance from current to solution states, and real distance already browsed.

To use them, look at this:
```py
solver = BreadthFirstSearchAlgorithm()

# Native solving
result : SearchResult = solver.search( TaquinProblem( init_with = [ 1, 2, 4, 3, 6, 7, 0, 8, 9 ] ) )

# Greedy solving
result : SearchResult = solver.search( GreedOptimizer( TaquinProblem( ... ), h = ManathanHeuristic() ) )

# AStart solving
result : SearchResult = solver.search( AStarOptimizer( TaquinProblem( ... ), g = DepthHeurstic(), h = ManathanHeuristic() ) )
```

## Defeat a player with your IA in 1v1 game

You want to prove to your best friend that you dominates him at Tic-Tac-Toe ? But you're the bad at it ?
Let's implement an IA to help you ;)

### Step 1: Define the problem
Firstly, we must define the game to play with. It's a simple implementation we few required improvements.
But it would be enough for our need:

```py
from simpleai.core.generic import State

# we assign a number for each player
CROSS = 1
ROUND = -1
EMPTY = 0
class MorpionsGame:
    def __init__( self, init_with : [int] = None ):
        # let's the opportunity to initiate the game with anything you want
        if init_with is None:
            self.plate = [ EMPTY for i in range( 9 ) ]
        else:
            self.plate = init_with

    def cross( self, x, y ):
        ''' Puts a cross at specified position. '''
        copy = MorpionsGame()
        copy.plate = self.plate[:]
        copy.__set( x, y, CROSS )
        return copy

    def round( self, x, y ):
        ''' Puts a round at specified positions. '''
        copy = MorpionsGame()
        copy.plate = self.plate[:]
        copy.__set( x, y, ROUND )
        return copy

    def equals( self, x, y, value ):
        ''' Returns True if cell at specified position is equals to given value. '''
        return self.plate[ 3 * x + y ] == value

    def __get( self, x, y ):
        ''' Returns cell's value located at specified positions. '''
        return self.plate[ 3 * x + y ]

    def __set( self, x, y, value ):
        ''' Change cell's value located at specified positions. '''
        self.plate[ 3 * x + y ] = value

    
    def playable( self ):
        ''' Returns playable positions as a list of tuple which contains two integers x and y. '''
        playable_coords = []
        for x in range( 3 ):
            for y in range( 3 ):
                if self.equals( x, y, EMPTY ):
                    playable_coords.append( (x, y) )
        return playable_coords

    def someone_has_won( self ):
        ''' Returns True if someone has won, False otherwise. '''
        # a solution is founded if there is a three-length cross or round line or diagonale
        # checks solutions on line
        for x in range( 3 ):
            index = 3 * x
            if self.plate[ index ] == self.plate[ index + 1 ] and self.plate[ index + 1 ] == self.plate[ index + 2 ] and self.plate[ index ] != EMPTY:
                return True

        # checks solutions on column
        for y in range( 3 ):
            if self.plate[ y ] == self.plate[ 3 + y ] and self.plate[ 3 + y ] == self.plate[ 6 + y ] and self.plate[ y ] != EMPTY:
                return True

        # checks solutions in diagonale
        if self.plate[ 0 ] == self.plate[ 4 ] and self.plate[ 4 ] == self.plate[ 8 ] and self.plate[ 0 ] != EMPTY:
            return True
        
        if self.plate[ 2 ] == self.plate[ 4 ] and self.plate[ 4 ] == self.plate[ 6 ] and self.plate[ 2 ] != EMPTY:
            return True

        return False
    
    def is_playable( self ):
        ''' Returns True if game is playable, or in other words if at least one cell is empty. '''
        return self.playable() != []
    
    def is_end( self ):
        ''' Returns True if game is ended, False otherwise. '''
        # the game is end when someone has won
        # of if no one cell is playable
        return self.someone_has_won() or not self.is_playable()

    def ask_playable( self ) -> ( i
```

Then we create the class `MorpionsProblem` to create a problem instance.
Notice that this clas inherit from `AdversarialProblem` which is the abstact class
used to deal with problem.

```py
from simpleai.core.adversarial.algorithms import AdversarialProblem

class MorpionsProblem( AdversarialProblem ):
    def initial_state(self):
        return State( MorpionsGame() )

    def successors( self, state, max_player ):
        succ = []
        for playable in state.data.playable():
            x, y = playable
            if max_player:
                succ.append( State( state.data.cross( x, y ) ) )
            else:
                succ.append( State( state.data.round( x, y ) ) ) 
        return succ
    
    def is_solution( self, state, max_player ):
        return state.data.is_end()
```

Now we have the game and a problem. We need to create an heuristic to evaluate each terminal node
in the tree used in [MinMax](https://en.wikipedia.org/wiki/Minimax) and [AlphaBeta](https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning) algorithms.

```py
class MorpionsHeuristic( AdversarialHeuristic ):
    def score(self, state, max_player):
        # If max player is a terminal player, it's a bad news => -1
        # otherwise, it's a good news :) => +1
        # notice the division by the current depth which allows to select the shortest
        # way to move on the solution.
        return -1 if max_player else 1 / state.depth
```

Now we are ready to play with our IA !

```py
from simpleai.core.adversarial.algorithms import MinMaxAlgorithm, AdversarialMove

problem = MorpionsProblem()
solver = MinMaxAlgorithm( problem, MorpionsHeuristic() )
game = problem.initial_state().data # We need to manipulate the game as MorpionsGame, not State object
while True:
    print( game )
    # checks if the game is end or not
    if game.is_end():
        if game.someone_has_won():
            if human_turn:
                print( "IA: I'm sorry, but you lose :(" )
            else:
                print( "IA: Wow, great job !" )
        else:
            print( "IA: It's seems there is no winner..." )
        break

    # play the game
    if human_turn:
        print( "-------------- Your turn -----------------" )
        x, y = game.ask_playable()
        game = game.round( x, y )
    else:
        print( "--------------  IA turn  -----------------" )
        print( "IA: Let me thinking about my next move..." )
        picked_move : AdversarialMove = solver.best_move( game )
        next_game : MorpionsGame = picked_move.move() # move returned data object
        game.plate = next_game.plate[:]
        print( "IA: Eureka !" )
```

You're ready to defeat your friend !
